from pathlib import Path

import matplotlib.gridspec as gridspec
import matplotlib.pyplot as plt
import pandas as pd
import skimage.io

import lib.figure_util as figure_util
import lib.filedb
import subfig_gradient
import subfig_trace

this_dir = Path(__file__).resolve().parent
figure_util.apply_style()


fig = plt.figure()
# When with was  figure_width big then  this was perfect
# 17.73cm wide 8.87cm high
# width, height = figure_util.get_figsize(figure_util.fig_width_big_pt, wf=1.0, hf=0.5 )
# gs = gridspec.GridSpec(2, 2, width_ratios=[0.3, 0.7], height_ratios=[0.55, 0.45])

# Making less wide
# width, height = figure_util.cm2inch(14, 8.87)
outer_gs = gridspec.GridSpec(
    2, 2, height_ratios=[1, 2.2], hspace=0.18, wspace=0.25, width_ratios=[0.7, 1]
)
pic_trace_gs = gridspec.GridSpecFromSubplotSpec(
    2, 1, height_ratios=[2, 1], subplot_spec=outer_gs[1, :], hspace=0.03
)

ax_exprmt = plt.subplot(outer_gs[0, 0])
ax_gradnt = plt.subplot(outer_gs[0, 1])
ax_fstrip = plt.subplot(pic_trace_gs[0, :])
ax_pulses = plt.subplot(pic_trace_gs[1, :])

################
## experiment figure
#################
ax_exprmt.spines["top"].set_visible(True)
ax_exprmt.spines["bottom"].set_visible(True)
ax_exprmt.spines["left"].set_visible(True)
ax_exprmt.spines["right"].set_visible(True)
ax_exprmt.axes.get_xaxis().set_ticks([])
ax_exprmt.axes.get_yaxis().set_ticks([])


# BF movie traces
base_dir = this_dir.parent.parent / "datasets" / "iphox_singlecell" / "BF10_timelapse"
movie = "Column_2"
comp_path = base_dir / movie / "compiled_redo.tsv"
ct_path = base_dir / movie / "cell_track.json"
compiled_trace, cell_tracks = subfig_trace.load_data(comp_path, ct_path)
compiled_trace["time"] = compiled_trace["time"] / 60  # hours
ax_pulses = subfig_trace.get_figure(ax_pulses, compiled_trace, cell_tracks)
ax_pulses.set_ylabel("YFP/RFP ratio")
ax_pulses.set_xlim(13.6, right=50)
ax_pulses.set_ylim(0, 1.1)

################
## Gradient
###############
dataset_dir = this_dir.parent.parent / "datasets" / "iphox_gradient_snaps"
file_df = lib.filedb.get_filedb(dataset_dir / "filedb.tsv")
df = pd.read_hdf(dataset_dir / "gradient_data_distmap.h5")
# df["g_by_r"] = df["green_bg_mean"]/df["red_bg_mean"]
times = file_df["time"].unique()
# times = [24, 48, 72, 96]

plotset = {"linewidth": 0.6, "alpha": 0.3}
ax_gradnt = subfig_gradient.get_figure(
    ax_gradnt, df, file_df, "green_bg_mean", "", "", times, plotset
)
leg = ax_gradnt.legend()
ax_gradnt, leg = figure_util.shift_legend(ax_gradnt, leg, xshift=0.08, yshift=0.145)
ax_gradnt.ticklabel_format(style="sci", scilimits=(1, 3), axis="both")
# ax_gradnt.xaxis.major.formatter._useMathText = True
ax_gradnt.set_xlim(0, 150)
ax_gradnt.set_ylim(0, 2900)
# ax_gradnt.set_xlabel("Distance from top of biofilm (μm)")
ax_gradnt.set_xlabel("Distance from top of biofilm ($\mu$m)")
ax_gradnt.set_ylabel("YFP (AU)", labelpad=-0.40)

###################
## film strip
##################
im = skimage.io.imread(this_dir.parent.parent / "datasets" / "figure_data" /"figure_bf_movie"/"delru_bf10_col2_strip.png")
ax_fstrip.imshow(im, interpolation="bicubic")
ax_fstrip.grid(False)
ax_fstrip.axis("off")

axes = [ax_exprmt, ax_gradnt, ax_fstrip, ax_pulses]
letter_style = {
    "verticalalignment": "top",
    "horizontalalignment": "right",
    "fontsize": figure_util.letter_font_size,
    # "color": "red"
}

letter_x = 0.03
axes[0].text(letter_x, 0.995, "A", transform=fig.transFigure, **letter_style)
axes[1].text(0.47, 0.995, "B", transform=fig.transFigure, **letter_style)
axes[2].text(letter_x, 0.63, "C", transform=fig.transFigure, **letter_style)
axes[3].text(letter_x, 0.26, "D", transform=fig.transFigure, **letter_style)


filename = "bf_movie_main"
# width, height = figure_util.get_figsize(figure_util.fig_width_big_pt, wf=1.0, hf=0.5 )
width, height = figure_util.get_figsize(figure_util.fig_width_small_pt, wf=1.0, hf=1.3)
fig.subplots_adjust(
    left=0.095, right=0.97, top=0.97, bottom=0.06
)  # , hspace=0.25, wspace=0.25)

fig.set_size_inches(width, height)  # common.cm2inch(width, height))
figure_util.save_figures(fig, filename, ["png", "pdf"], base_dir=this_dir)
