import numpy as np
import lib.cell_tracking.track_data as track_data
from lib.cell_tracking.track_data import TrackData
import pandas as pd
import lib.figure_util as figure_util
import matplotlib.pyplot as plt

import scipy.stats


def load_data(compiled_data_path, cell_track_path):
    td = TrackData(cell_track_path)
    df = pd.read_csv(compiled_data_path, sep="\t")
    return df, td


metrics = {
    "mean": lambda x: x.mean(),
    "std": lambda x: x.std(),
    "CV": scipy.stats.variation,
}


def plot_hist(ax, df, chan, bins, **kwargs):
    # cell = df[df["cell_id"].isin(cell_ids)].sort_values(by=["cell_id", "frame"])
    values = df[chan].dropna()
    for name, func in metrics.items():
        print(name, func(values.values))
    # values.to_csv(f"source_data/sup_figure7_{chan}.tsv", sep="\t", index=None) 
    counts, bins = np.histogram(values, bins=bins)
    counts = (counts / len(values)) * 100
    cbins = bins[1:] - ((bins[1] - bins[0]) / 2)
    # times = cell.groupby("frame").mean()["time"]
    # if "color" not in kwargs: kwargs["color"] = np.random.rand(3)
    # if "label" not in kwargs: kwargs["label"] = str(cell_ids[-1])
    ax.barh(cbins, width=counts, height=80, **kwargs)
    ax.set_xlabel("Percentage of cells")
    return ax


def get_figure(ax, compiled, bins, channel, **kwargs):
    ax = plot_hist(ax, compiled, channel, bins, **kwargs)

    return ax


def main():
    import os.path

    base_dir = "datasets/iphox_singlecell/BF10_timelapse/"
    # base_dir="proc_data/iphox_movies/BF10_timelapse/"
    # base_dir =  "datasets_offline/iphox_singlecell/BF10_timelapse/"
    movie = "Column_2"
    comp_path = os.path.join(base_dir, movie, "compiled_redo.tsv")
    ct_path = os.path.join(base_dir, movie, "cell_track.json")
    compiled_trace, cell_tracks = load_data(comp_path, ct_path)

    print(compiled_trace.dropna().groupby("frame").count())
    # compiled_trace = compiled_trace[compiled_trace["frame"]!=50]# dropna().groupby("frame").count()
    compiled_trace = compiled_trace[
        compiled_trace["frame"] == 50
    ]  # dropna().groupby("frame").count()

    fig, ax = plt.subplots(1, 1)
    color, ylab, cmax = "green", "P$_{sigB}$-YFP (AU)", 3500
    chan = color
    opts = {"color": color}
    bins = np.arange(0, 6000, 120)
    ax = get_figure(ax, compiled_trace, bins, chan, **opts)
    ax.set_ylabel(ylab)
    ax.set_ylim(bottom=0, ymax=cmax)
    ax.ticklabel_format(style="sci", scilimits=(1, 3), axis="y", useMathText=True)
    ax.set_xlim(0, right=10)
    ax.tick_params(axis="y", which="both", direction="out")  #
    ax.yaxis.labelpad = -1

    plt.show()


if __name__ == "__main__":
    main()

